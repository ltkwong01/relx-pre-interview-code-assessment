package com.example.relx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RelxApplication {

	public static void main(String[] args) {
		SpringApplication.run(RelxApplication.class, args);
	}

}
