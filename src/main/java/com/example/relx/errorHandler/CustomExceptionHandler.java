package com.example.relx.errorHandler;

import com.example.relx.exceptions.ElementNotFoundException;
import com.example.relx.exceptions.InputErrorException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
@Slf4j
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {
    // constructor
    public CustomExceptionHandler() {
        super();
    }

    @ExceptionHandler({ ElementNotFoundException.class })
    public ResponseEntity<Object> handleElementNotFound(final ElementNotFoundException ex, final WebRequest request) {
        return handleExceptionInternal(ex, ex.getErrorDetail(), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler({ InputErrorException.class })
    public ResponseEntity<Object> handleInvalidInput(final InputErrorException ex, final WebRequest request) {
        log.error("input got some error");
        return handleExceptionInternal(ex, ex.getErrorDetail(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler({ IllegalArgumentException.class })
    public ResponseEntity<Object> handleInternal(final IllegalArgumentException ex, final WebRequest request) {
        logger.error("Wrong argument", ex);
        final String bodyOfResponse = "Please supply correct argument";
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    @ExceptionHandler({Exception.class})
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        logger.error("Error", ex);
        final String bodyOfResponse = "Error";
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }


}
