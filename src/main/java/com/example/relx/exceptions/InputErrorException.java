package com.example.relx.exceptions;

import lombok.Getter;

public class InputErrorException extends Exception{
    @Getter
    GeneralError errorDetail;

    public InputErrorException(GeneralError generalError){
        super();

        this.errorDetail = generalError;
    }
}
