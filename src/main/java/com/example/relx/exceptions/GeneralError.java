package com.example.relx.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class GeneralError {
    @Getter
    @Setter
    String errorCode;

    @Getter
    @Setter
    String errorMessage;
}
