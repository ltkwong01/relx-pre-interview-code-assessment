package com.example.relx.exceptions;

import lombok.Getter;

public class ElementNotFoundException extends Exception{
    @Getter
    GeneralError errorDetail;

    public ElementNotFoundException(GeneralError generalError){
        super();

        this.errorDetail = generalError;
    }
}
