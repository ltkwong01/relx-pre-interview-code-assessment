package com.example.relx.service.impl;

import com.example.relx.exceptions.ElementNotFoundException;
import com.example.relx.exceptions.GeneralError;
import com.example.relx.exceptions.InputErrorException;
import com.example.relx.model.Element;
import com.example.relx.model.response.BriefSingleElement;
import com.example.relx.model.response.DetailSingleElement;
import com.example.relx.service.ElementService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
@Slf4j
public class ElementServiceImpl implements ElementService {
    @Autowired
    JsonServiceImpl jsonService;

    @Override
    public ArrayList<BriefSingleElement> getAllElements() throws IOException {
        log.info("Calling getAllElements()");
        ArrayList<BriefSingleElement> briefSingleElementArrayList = new ArrayList<>();

        for(Element element: jsonService.readAndParseJson()){
            briefSingleElementArrayList.add(new BriefSingleElement(element.getName(), element.getAtomicNumber()));
        }
        return briefSingleElementArrayList;
    }

    @Override
    public ArrayList<BriefSingleElement> getFilteredElements(Integer group, Integer period) throws IOException {
        log.info("Calling getFilteredElements()");
        ArrayList<BriefSingleElement> briefSingleElementArrayList = new ArrayList<>();

        // retrieve all elements from json file first
        List<Element> elements = Arrays.asList(jsonService.readAndParseJson()); ;

        // filter by group ONLY when group have some value
        if(!Objects.isNull(group)){
            log.info(String.format("Filtering with group number [%d]", group));
            elements = elements.stream()
                    .filter(element -> element.getGroupNumber() == group)
                    .collect(Collectors.toList());
        }

        // filter by period when period have some value
        if(!Objects.isNull(period)){
            log.info(String.format("Filtering with period number [%d]", period));
            elements = elements.stream()
                    .filter(element -> element.getPeriod() == period)
                    .collect(Collectors.toList());
        }

        // construct answer
        briefSingleElementArrayList = (ArrayList<BriefSingleElement>) elements.stream().map(element -> new BriefSingleElement(
                element.getName(), element.getAtomicNumber()
        )).collect(Collectors.toList());

        return briefSingleElementArrayList;
    }

    @Override
    public DetailSingleElement getDetailedElement(Integer atomicNumber) throws ElementNotFoundException, InputErrorException, IOException {
        log.info("Calling getDetailedElement()");
        log.info(String.format("Try to get detail for element with atomic number [%d]", atomicNumber));

        // check if user enter something
        if(Objects.isNull(atomicNumber)){
            throw new InputErrorException(new GeneralError("InputError", "Input is null"));
        }

        // find element with atomic number, if we cannot find element with atomicNumber, set as null
        Element selectedElement = Arrays.stream(jsonService.readAndParseJson())
                                        .filter(element -> atomicNumber.equals(element.getAtomicNumber()))
                                        .findFirst()
                                        .orElse(null);

        // throw error if element not found
        if (Objects.isNull(selectedElement)){
            log.error(String.format("Cannot find element with atomic number [%d]", atomicNumber));
            throw new ElementNotFoundException(new GeneralError("ElementNotFound", String.format("Cannot found element with atomic number [%d]", atomicNumber)));
        }

        // prepare result
        DetailSingleElement result = new DetailSingleElement();
        result.setAtomicNumber(selectedElement.getAtomicNumber());
        result.setName(selectedElement.getName());
        result.setAlternativeName(StringUtils.isEmpty(selectedElement.getAlternativeName()) ? "none" : selectedElement.getAlternativeName());
        result.setSymbol(selectedElement.getSymbol());
        result.setAppearance(selectedElement.getAppearance());
        result.setDiscoverers(this.getDiscoverers(selectedElement.getDiscovery()));
        result.setDiscoveryYear(this.getDiscoveryYear(selectedElement.getDiscovery()));
        result.setGroup(selectedElement.getGroupNumber());
        result.setPeriod(selectedElement.getPeriod());

        return result;
    }

    private String getDiscoveryYear(String discoveryString){
        final String UNKNOWN = "unknown";
        if (Objects.isNull(discoveryString)){
            return UNKNOWN;
        }

        Pattern p = Pattern.compile("\\((.*?)\\)"); // extract string inside brackets
        Matcher n = p.matcher(discoveryString);

        if (n.find()) {
            return n.group(1);
        }else{
            return UNKNOWN;
        }
    }

    private String[] getDiscoverers(String discoveryString){
        if (Objects.isNull(discoveryString)){
            return null;
        }

        // separate name(s) and discover year
        String names = discoveryString.split ("\\(", 2)[0];

        String[] splittedNameByComma = names.split (",");
        String[] splittedNameByAnd = names.split (" and ");

        if (splittedNameByComma.length > 1){
            log.debug("using comma");

            // trim string inside array when return
            return Arrays.stream(splittedNameByComma).map(String::trim).collect(Collectors.toList()).toArray(new String[0]);
        }else if(splittedNameByAnd.length > 1){
            log.debug("using and");

            // trim string inside array when return
            return Arrays.stream(splittedNameByAnd).map(String::trim).collect(Collectors.toList()).toArray(new String[0]);
        }
        else{
            log.debug("Single person");
            return new String[]{names.trim()};
        }
    }
}
