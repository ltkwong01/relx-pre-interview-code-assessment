package com.example.relx.service.impl;

import com.example.relx.deserializer.ElementDeserializer;
import com.example.relx.model.Element;
import com.example.relx.service.JsonService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;


@Slf4j
@Component
public class JsonServiceImpl implements JsonService {

    @Override
    @Cacheable({"elements"}) // cache the parsed result as it won't change a lot
    public Element[] readAndParseJson() throws IOException{

        // get json file
        ClassPathResource staticDataResource = new ClassPathResource("periodic_table.json");
        String staticDataString = IOUtils.toString(staticDataResource.getInputStream(), StandardCharsets.UTF_8);


        // cast to Element type by using custom deserializer
        Gson gson = new GsonBuilder().registerTypeAdapter(Element[].class, new ElementDeserializer()).create();
        Element[] elements = gson.fromJson(staticDataString, Element[].class);

        return elements;
    }
}
