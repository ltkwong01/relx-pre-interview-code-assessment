package com.example.relx.service;

import com.example.relx.exceptions.ElementNotFoundException;
import com.example.relx.exceptions.InputErrorException;
import com.example.relx.model.response.BriefSingleElement;
import com.example.relx.model.response.DetailSingleElement;

import java.io.IOException;
import java.util.ArrayList;

public interface ElementService {
    public ArrayList<BriefSingleElement> getAllElements() throws IOException;

    public ArrayList<BriefSingleElement> getFilteredElements(Integer group, Integer period) throws IOException;

    public DetailSingleElement getDetailedElement(Integer atomicNumber) throws ElementNotFoundException, InputErrorException, IOException;
}
