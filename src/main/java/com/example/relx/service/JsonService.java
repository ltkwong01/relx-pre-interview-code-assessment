package com.example.relx.service;

import com.example.relx.model.Element;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface JsonService {
    public Element[] readAndParseJson() throws IOException;
}
