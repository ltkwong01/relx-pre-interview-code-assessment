package com.example.relx.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Element {
    @SerializedName("allotropes")
    String allotropes;

    @SerializedName("alternative_name")
    String alternativeName;

    @SerializedName("alternative_names")
    String alternativeNames;

    @SerializedName("appearance")
    String appearance;

    @SerializedName("at_t_(k)")
    Integer atKevinTemperature;

    @SerializedName("atomic_number")
    Integer atomicNumber;

    @SerializedName("atomic_radius")
    String atomicRadius;

    @SerializedName("atomic_weight")
    Double atomicWeight;

    @SerializedName("band_gap")
    Double bandGap;

    @SerializedName("boiling_point")
    String boilingPoint; // combine with both string and number

    @SerializedName("brinell_hardness")
    String brinellHardness; // combine with both string and number

    @SerializedName("bulk_modulus")
    String bulkModulus; // combine with both string and number

    @SerializedName("cas_number")
    String casNumber;

    @SerializedName("color")
    String color;

    @SerializedName("covalent_radius")
    String covalentRadius; // combine with both string and number

    @SerializedName("critical_point")
    Double criticalPoint;

    @SerializedName("crystal_structure")
    String crystalStructure;

    @SerializedName("curie_point")
    Integer curiePoint; // not sure can be Integer ot not

    @SerializedName("density_at_stp")
    Double densityAtStp;

    @SerializedName("density_near_rt")
    String densityNearRt; // combine with both string and number

    @SerializedName("density_when_liquid_at_mp")
    String densityWhenLiquidAtMp;

    @SerializedName("discovery")
    String discovery;

    @SerializedName("discovery_and_first_isolation")
    String discoveryAndFirstIsolation;

    @SerializedName("electrical_resistivity")
    String electricalResistivity; // combine with both string and number

    @SerializedName("electron_configuration")
    String electronConfiguration;

    @SerializedName("electronegativity")
    String electronegativity;

    @SerializedName("element_category")
    String elementCategory;

    @SerializedName("first_isolation")
    String firstIsolation;

    @SerializedName("group_block")
    String groupBlock;

    @SerializedName("heat_of_fusion")
    String heatOfFusion; // combine with both string and number

    @SerializedName("heat_of_vaporisation")
    String heatOfVaporisation; // combine with both string and number

    @SerializedName("heat_of_vaporization")
    String heatOfVaporization; // combine with both string and number

    @SerializedName("ionisation_energies")
    String ionisationEnergies;

    @SerializedName("ionization_energies")
    String ionizationEnergies;

    @SerializedName("iso")
    String iso;

    @SerializedName("magnetic_ordering")
    String magneticOrdering;

    @SerializedName("melting_point")
    String meltingPoint; // combine string and number

    @SerializedName("mohs_hardness")
    String mohsHardness; // combine string and number

    @SerializedName("molar_heat_capacity")
    String molarHeatCapacity; // combine string and number

    @SerializedName("molar_volume")
    String molarVolume;

    @SerializedName("name")
    String name;

    @SerializedName("name_symbol")
    String nameSymbol;

    @SerializedName("named_by")
    String namedBy;

    @SerializedName("naming")
    String naming;

    @SerializedName("number")
    Integer number;

    @SerializedName("oxidation_states")
    String oxidationStates; // combine string and number

    @SerializedName("p_(pa)")
    String pressureInPa;

    @SerializedName("per_shell")
    Integer perShell;

    @SerializedName("period")
    Integer period;

    @SerializedName("phase")
    String phase;

    @SerializedName("poisson_ratio")
    String poissonRatio; // combine string and number

    @SerializedName("prediction")
    String prediction;

    @SerializedName("pronunciation")
    String pronunciation;

    @SerializedName("proposed_formal_name")
    String proposedFormalName;

    @SerializedName("recognised_as_an_element_by")
    String recognisedAsAnElementBy; // Phosphorus don't have this value

    @SerializedName("recognized_as_adistinct_element_by")
    String recognizedAsADistinctElementBy;

    @SerializedName("recognized_as_aunique_metal_by")
    String recognizedAsAUniqueMetalBy;

    @SerializedName("recognized_as_an_element_by")
    String recognizedAsAnElementBy;

    @SerializedName("shear_modulus")
    String shearModulus; // combine string and number

    @SerializedName("speed_of_sound")
    String speedOfSound; // combine string and number

    @SerializedName("speed_of_sound_thin_rod")
    String speedOfSoundThinRod;

    @SerializedName("sublimation_point")
    Double sublimationPoint;

    @SerializedName("symbol")
    String symbol;

    @SerializedName("tensile_strength")
    String tensileStrength;

    @SerializedName("thermal_conductivity")
    String thermalConductivity; // combine string and number

    @SerializedName("thermal_diffusivity")
    Double thermalDiffusivity;

    @SerializedName("thermal_expansion")
    String thermalExpansion; // combine string and number

    @SerializedName("triple_point")
    Double triplePoint;

    @SerializedName("van_der_waals_radius")
    Integer vanDerWaalsRadius;

    @SerializedName("vickers_hardness")
    String vickersHardness; // combine string and number

    @SerializedName("when_liquid_at_bp")
    Double whenLiquidAtBp;

    @SerializedName("when_liquid_at_mp")
    Double whenLiquidAtMp;

    @SerializedName("youngs_modulus")
    String youngsModulus; // combine string and number

    Integer groupNumber; // converted from groupBlock in deserializer
}
