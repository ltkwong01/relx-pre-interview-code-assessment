package com.example.relx.model.response;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DetailSingleElement {
    Integer atomicNumber;
    String name;
    String alternativeName;
    String symbol;
    String appearance;
    String[] discoverers;
    String discoveryYear;
    Integer group;
    Integer period;
}
