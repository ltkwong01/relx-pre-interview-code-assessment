package com.example.relx.model.response;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BriefSingleElement {
    String name;
    Integer atomicNumber;
}
