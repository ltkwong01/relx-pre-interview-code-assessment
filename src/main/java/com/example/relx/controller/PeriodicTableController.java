package com.example.relx.controller;

import com.example.relx.exceptions.ElementNotFoundException;
import com.example.relx.exceptions.InputErrorException;
import com.example.relx.model.response.BriefSingleElement;
import com.example.relx.model.response.DetailSingleElement;
import com.example.relx.service.impl.ElementServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;


@RestController
@Slf4j
@RequestMapping(value = "/periodicTable")
public class PeriodicTableController {
    @Autowired
    ElementServiceImpl elementService;

    @GetMapping("/getAllElements")
    public ArrayList<BriefSingleElement> getAllElements() throws IOException {
        log.info("GET request for ALL elements");
        return elementService.getAllElements();
    }

    @GetMapping("/getFilteredElements")
    public ArrayList<BriefSingleElement> getFilteredElements(@RequestParam(required = false) Integer group, @RequestParam(required = false) Integer period) throws IOException {
        log.info("GET request for filtered elements");
        return elementService.getFilteredElements(group, period);
    }

    @GetMapping("/getElementDetails/{atomicNumber}")
    public DetailSingleElement getElementDetails(@PathVariable Integer atomicNumber) throws InputErrorException, ElementNotFoundException, IOException {
        log.info("GET request for element detail");
        return elementService.getDetailedElement(atomicNumber);
    }
}
