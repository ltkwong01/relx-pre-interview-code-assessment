package com.example.relx.deserializer;

import com.example.relx.model.Element;
import com.google.gson.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class ElementDeserializer implements JsonDeserializer<Element[]> {
    // A custom deserializer for handling periodic table in .json file

    @Override
    public Element[] deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext ctx) throws JsonParseException
    {
        ArrayList<Element> elements = new ArrayList<>();
        JsonArray jsonArray = json.getAsJsonArray();

        for (JsonElement jsonElement: jsonArray) {
            Map<String, JsonElement> jsonMap = jsonElement.getAsJsonObject().asMap();

            // define what to be null
            String allotropes = this.convertToString(jsonMap.get("allotropes"));
            String alternativeName = this.convertToString(jsonMap.get("alternative_name"));
            String alternativeNames = this.convertToString(jsonMap.get("alternative_names"));
            String appearance = this.convertToString(jsonMap.get("appearance"));
            Integer atKevinTemperature = this.convertToInteger(jsonMap.get("at_t_(k)"));
            Integer atomicNumber = this.convertToInteger(jsonMap.get("atomic_number"));
            String atomicRadius = this.convertToString(jsonMap.get("atomic_radius"));
            Double atomicWeight = this.convertToDouble(jsonMap.get("atomic_weight"));
            Double bandGap = this.convertToDouble(jsonMap.get("band_gap"));
            String boilingPoint = this.convertToString(jsonMap.get("boiling_point")); // combine with both string and number
            String brinellHardness = this.convertToString(jsonMap.get("brinell_hardness")); // combine with both string and number
            String bulkModulus = this.convertToString(jsonMap.get("bulk_modulus")); // combine with both string and number
            String casNumber = this.convertToString(jsonMap.get("cas_number"));
            String color = this.convertToString(jsonMap.get("color"));
            String covalentRadius = this.convertToString(jsonMap.get("covalent_radius")); // combine with both string and number
            Double criticalPoint = this.convertToDouble(jsonMap.get("critical_point"));
            String crystalStructure = this.convertToString(jsonMap.get("crystal_structure"));
            Integer curiePoint = this.convertToInteger(jsonMap.get("curie_point")); // not sure can be Integer ot not
            Double densityAtStp = this.convertToDouble(jsonMap.get("density_at_stp"));
            String densityNearRt = this.convertToString(jsonMap.get("density_near_rt")); // combine with both string and number
            String densityWhenLiquidAtMp = this.convertToString(jsonMap.get("density_when_liquid_at_mp"));
            String discovery = this.convertToString(jsonMap.get("discovery"));
            String discoveryAndFirstIsolation = this.convertToString(jsonMap.get("discovery_and_first_isolation"));
            String electricalResistivity = this.convertToString(jsonMap.get("electrical_resistivity")); // combine with both string and number
            String electronConfiguration = this.convertToString(jsonMap.get("electron_configuration"));
            String electronegativity = this.convertToString(jsonMap.get("electronegativity"));
            String elementCategory = this.convertToString(jsonMap.get("element_category"));
            String firstIsolation = this.convertToString(jsonMap.get("first_isolation"));
            String groupBlock = this.convertToString(jsonMap.get("group_block"));
            String heatOfFusion = this.convertToString(jsonMap.get("heat_of_fusion")); // combine with both string and number
            String heatOfVaporisation = this.convertToString(jsonMap.get("heat_of_vaporisation")); // combine with both string and number
            String heatOfVaporization = this.convertToString(jsonMap.get("heat_of_vaporization")); // combine with both string and number
            String ionisationEnergies = this.convertToString(jsonMap.get("ionisation_energies"));
            String ionizationEnergies = this.convertToString(jsonMap.get("ionization_energies"));
            String iso = this.convertToString(jsonMap.get("iso"));
            String magneticOrdering = this.convertToString(jsonMap.get("magnetic_ordering"));
            String meltingPoint = this.convertToString(jsonMap.get("melting_point")); // combine string and number
            String mohsHardness = this.convertToString(jsonMap.get("mohs_hardness")); // combine string and number
            String molarHeatCapacity = this.convertToString(jsonMap.get("molar_heat_capacity")); // combine string and number
            String molarVolume = this.convertToString(jsonMap.get("molar_volume"));
            String name = this.convertToString(jsonMap.get("name"));
            String nameSymbol = this.convertToString(jsonMap.get("name_symbol"));
            String namedBy = this.convertToString(jsonMap.get("named_by"));
            String naming = this.convertToString(jsonMap.get("naming"));
            Integer number = this.convertToInteger(jsonMap.get("number"));
            String oxidationStates = this.convertToString(jsonMap.get("oxidation_states")); // combine string and number
            String pressureInPa = this.convertToString(jsonMap.get("pressure_in_pa"));
            Integer perShell = this.convertToInteger(jsonMap.get("per_shell"));
            Integer period = this.convertToInteger(jsonMap.get("period"));
            String phase = this.convertToString(jsonMap.get("phase"));
            String poissonRatio = this.convertToString(jsonMap.get("poisson_ratio"));
            String prediction = this.convertToString(jsonMap.get("prediction"));
            String pronunciation = this.convertToString(jsonMap.get("pronunciation"));
            String proposedFormalName = this.convertToString(jsonMap.get("proposed_formal_name"));
            String recognisedAsAnElementBy = this.convertToString(jsonMap.get("recognised_as_an_element_by")); // Phosphorus don't have this value
            String recognizedAsADistinctElementBy = this.convertToString(jsonMap.get("recognized_as_adistinct_element_by"));
            String recognizedAsAUniqueMetalBy = this.convertToString(jsonMap.get("recognized_as_aunique_metal_by"));
            String recognizedAsAnElementBy = this.convertToString(jsonMap.get("recognized_as_an_element_by"));
            String shearModulus = this.convertToString(jsonMap.get("shear_modulus"));
            String speedOfSound = this.convertToString(jsonMap.get("speed_of_sound")); // combine string and number
            String speedOfSoundThinRod = this.convertToString(jsonMap.get("speed_of_sound_thin_rod"));
            Double sublimationPoint = this.convertToDouble(jsonMap.get("sublimation_point"));
            String symbol = this.convertToString(jsonMap.get("symbol"));
            String tensileStrength = this.convertToString(jsonMap.get("tensile_strength"));
            String thermalConductivity = this.convertToString(jsonMap.get("thermal_conductivity")); // combine string and number
            Double thermalDiffusivity = this.convertToDouble(jsonMap.get("thermal_diffusivity"));
            String thermalExpansion = this.convertToString(jsonMap.get("thermal_expansion")); // combine string and number
            Double triplePoint = this.convertToDouble(jsonMap.get("triple_point"));
            Integer vanDerWaalsRadius = this.convertToInteger(jsonMap.get("van_der_waals_radius"));
            String vickersHardness = this.convertToString(jsonMap.get("vickers_hardness")); // combine string and number
            Double whenLiquidAtBp = this.convertToDouble(jsonMap.get("when_liquid_at_bp"));
            Double whenLiquidAtMp = this.convertToDouble(jsonMap.get("when_liquid_at_mp"));
            String youngsModulus = this.convertToString(jsonMap.get("youngs_modulus")); // combine string and number
            Integer groupNumber = this.getGroupNumberFromGroupBlock(groupBlock);

            elements.add(new Element(
                    allotropes,
                    alternativeName,
                    alternativeNames,
                    appearance,
                    atKevinTemperature,
                    atomicNumber,
                    atomicRadius,
                    atomicWeight,
                    bandGap,
                    boilingPoint,
                    brinellHardness,
                    bulkModulus,
                    casNumber,
                    color,
                    covalentRadius,
                    criticalPoint,
                    crystalStructure,
                    curiePoint,
                    densityAtStp,
                    densityNearRt,
                    densityWhenLiquidAtMp,
                    discovery,
                    discoveryAndFirstIsolation,
                    electricalResistivity,
                    electronConfiguration,
                    electronegativity,
                    elementCategory,
                    firstIsolation,
                    groupBlock,
                    heatOfFusion,
                    heatOfVaporisation,
                    heatOfVaporization,
                    ionisationEnergies,
                    ionizationEnergies,
                    iso,
                    magneticOrdering,
                    meltingPoint,
                    mohsHardness,
                    molarHeatCapacity,
                    molarVolume,
                    name,
                    nameSymbol,
                    namedBy,
                    naming,
                    number,
                    oxidationStates,
                    pressureInPa,
                    perShell,
                    period,
                    phase,
                    poissonRatio,
                    prediction,
                    pronunciation,
                    proposedFormalName,
                    recognisedAsAnElementBy,
                    recognizedAsADistinctElementBy,
                    recognizedAsAUniqueMetalBy,
                    recognizedAsAnElementBy,
                    shearModulus,
                    speedOfSound,
                    speedOfSoundThinRod,
                    sublimationPoint,
                    symbol,
                    tensileStrength,
                    thermalConductivity,
                    thermalDiffusivity,
                    thermalExpansion,
                    triplePoint,
                    vanDerWaalsRadius,
                    vickersHardness,
                    whenLiquidAtBp,
                    whenLiquidAtMp,
                    youngsModulus,
                    groupNumber));
        }
        return elements.toArray(new Element[0]);
    }

    private String convertToString(JsonElement jsonElement){
        if(jsonElement == null){
            return null;
        }

        String value = jsonElement.getAsString();
        if (StringUtils.equals("n/a", value)){
            return null;
        }
        return value;
    }

    private Integer convertToInteger(JsonElement jsonElement){
        if(jsonElement == null){
            return null;
        }

        String value = jsonElement.getAsString();
        if (StringUtils.equals("n/a", value)){
            return null;
        }

        // convert to Integer
        return Integer.parseInt(value);
    }

    private Double convertToDouble(JsonElement jsonElement){
        if(jsonElement == null){
            return null;
        }

        String value = jsonElement.getAsString();
        if (StringUtils.equals("n/a", value)){
            return null;
        }

        // convert to Double
        return Double.parseDouble(value);
    }

    private Integer getGroupNumberFromGroupBlock(String groupBlockString){
        Pattern pattern = Pattern.compile("group ([0-9]*)");
        Matcher matcher = pattern.matcher(groupBlockString);

        // try to extract group number or return zero if we cannot find
        if (matcher.find()) {
            if(StringUtils.isBlank(matcher.group(1))){
                return 0;
            }

            Integer groupNumber;
            try{
                groupNumber = Integer.valueOf(matcher.group(1));
            }catch (NumberFormatException nfe){
                // if we cannot convert to integer, also return 0
                log.error(nfe.getMessage());
                return 0;
            }
            log.debug(String.format("group number is %d", groupNumber));

            return groupNumber;
        }else{
            return 0;
        }
    }
}


