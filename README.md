## Pre-Interview Code test

### 1. Get a list of all elements on the periodic table
You can use the following command for request
```shell
curl --location --request GET 'https://pre-interview-app-svc-jasonk369.cloud.okteto.net/periodicTable/getAllElements'
```

### 2. Get filtered list of elements
You can filter by Group / Period or even both

#### Filter by Group only
```shell
curl --location --request GET 'https://pre-interview-app-svc-jasonk369.cloud.okteto.net/periodicTable/getFilteredElements?group=13'
```

#### Filter by Period only
```shell
curl --location --request GET 'https://pre-interview-app-svc-jasonk369.cloud.okteto.net/periodicTable/getFilteredElements?period=1'
```

#### Filter by both Group and Period
```shell
curl --location --request GET 'https://pre-interview-app-svc-jasonk369.cloud.okteto.net/periodicTable/getFilteredElements?group=13&period=3'
```

### 3. Get details for an individual element by atomic number
You can enter atomic number at the end
The format be like: `https://pre-interview-app-svc-jasonk369.cloud.okteto.net/periodicTable/getElementDetails/<atomicNumber>`
```shell
curl --location --request GET 'https://pre-interview-app-svc-jasonk369.cloud.okteto.net/periodicTable/getElementDetails/1'
```
